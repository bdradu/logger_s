// TestLogger.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "FileReader.h"


int main() {

	TimerManager::StartTimer("timer1");

	FileReader r("t.txt");
	std::cout << TimerManager::ReadTimer("timer1")<<std::endl;
	int sum = r.sumNumbersFromFile();
	TimerManager::StopTimer("timer1");

	return 0;
}