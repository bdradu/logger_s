#include "stdafx.h"
#include "FileReader.h"



FileReader::FileReader(std::string filePath)
{
	fin.open(filePath);
	if (fin.is_open())
	{
		ConsoleLog::getInstance()->Log(LoggingLevels::Logs::INFO, "am deschis fisierul");
		FileLog::getInstance("out.out")->Log(LoggingLevels::Logs::INFO, "am deschis fisierul");
		std::cout << TimerManager::ReadTimer("timer1") << std::endl;

	}
	else
	{
		ConsoleLog::getInstance()->Log(LoggingLevels::Logs::FATAL, "fisierul nu poate fi deschis");
		FileLog::getInstance("out.out")->Log(LoggingLevels::Logs::FATAL, "fisierul nu poate fi deschis");
	}

}

int FileReader::sumNumbersFromFile()
{
	int sum = 0;

	if (fin.is_open())
	{

		int value;

		while (fin >> value)
		{
			sum += value;
		}

		ConsoleLog::getInstance()->Log(LoggingLevels::Logs::INFO, "am calculat suma");
		FileLog::getInstance("out.out")->Log(LoggingLevels::Logs::INFO, "am calculat suma");

	}

	return sum;
}

FileReader::~FileReader()
{
	fin.close();
}
