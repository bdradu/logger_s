#pragma once
#include <string>
#include <fstream>
#include "../Logger/ConsoleLog.h"
#include "../Logger/LoggingLevels.h"
#include "../Logger/FileLog.h"
#include"../Timer/TimerManager.h"
class FileReader
{
	std::ifstream fin;
public:
	FileReader(std::string filePath);
	int sumNumbersFromFile();
	~FileReader();
};

