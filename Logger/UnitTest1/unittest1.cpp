#include "stdafx.h"
#include "CppUnitTest.h"
#include "../TestLogger/FileReader.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestSumReader)
		{
			auto reader = new FileReader("t.txt");
			auto expected = 0;
			auto result = reader->sumNumbersFromFile();
			Assert::AreEqual(expected, result);

		}

	};
}