#include "ConsoleLog.h"




ConsoleLog::ConsoleLog()
{
	
}


ConsoleLog::~ConsoleLog()
{
}

ConsoleLog * ConsoleLog::getInstance()
{
	static ConsoleLog* instance = new ConsoleLog();
	return instance;
}

void ConsoleLog::Log(LoggingLevels::Logs log, std::string message)
{
	auto start = std::chrono::system_clock::now();
	auto end_time = std::chrono::system_clock::to_time_t(start);
	std::stringstream ss;
	ss << std::put_time(std::localtime(&end_time), "%Y-%m-%d %X");
	std::string time = ss.str();
	std::cout << time << " " << LoggingLevels::LogsMap.at(log) << " " << message << std::endl;
}
