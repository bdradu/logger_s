#pragma once
#include "LoggingLevels.h"
const std::map<LoggingLevels::Logs, std::string> LoggingLevels::LogsMap = { {LoggingLevels::Logs::FATAL, "FATAL"}, { LoggingLevels::Logs::ERROR, "ERROR"}, { LoggingLevels::Logs::WARNING, "WARNING"},{ LoggingLevels::Logs::INFO, "INFO"},{ LoggingLevels::Logs::DEBUG, "DEBUG" } };