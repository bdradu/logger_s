#pragma once
#include <map>
class LoggingLevels {
public:
	enum class Logs {
		FATAL,
		ERROR,
		WARNING,
		INFO,
		DEBUG
	};
	static const std::map<Logs, std::string> LogsMap;
};