#pragma once

#include <iostream>
#include "LoggingLevels.h"

class ILog {
public:
	virtual void Log(LoggingLevels::Logs log, std::string message) = 0;
	ILog();
	virtual ~ILog();
};

