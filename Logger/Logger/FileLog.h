#pragma once

#include "ILog.h"
#include "LoggingLevels.h"

#include <fstream>
#include <array>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

class FileLog :
	public ILog {

private:
	std::string m_filePath;
private:
	FileLog(std::string filePath);
	~FileLog();
public:
	static FileLog* getInstance(std::string filePath);
	// Inherited via ILog
	virtual void Log(LoggingLevels::Logs log, std::string message) override;
};