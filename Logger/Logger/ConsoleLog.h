#pragma once
#include "ILog.h"
#include <iostream>
#include<string>

#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>
class ConsoleLog :
	public ILog
{

private:
	ConsoleLog();
	virtual ~ConsoleLog();
public:
	static ConsoleLog* getInstance();
	// Inherited via ILog
	virtual void Log(LoggingLevels::Logs log, std::string message) override;
};

