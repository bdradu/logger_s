#define _CRT_SECURE_NO_WARNINGS
#include "FileLog.h"

FileLog::FileLog(std::string filePath):m_filePath(filePath)
{
}

FileLog::~FileLog()
{
}

FileLog * FileLog::getInstance(std::string filePath)
{
	static FileLog* instance = new FileLog(filePath);
	return instance;
}

void FileLog::Log(LoggingLevels::Logs log, std::string message)
{
	std::ofstream fout;
	fout.open(m_filePath);
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
	std::string time = ss.str();

	fout << time<< " " << LoggingLevels::LogsMap.at(log)<< " " << message;


	fout.close();
}

