#include "stdafx.h"
#include "Timer.h"


Timer::Timer()
{
}


Timer::~Timer()
{
}

void Timer::Start()
{
	mStart = std::chrono::system_clock::now();
}

long long Timer::Lap()
{
	
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	std::chrono::duration<float> difference = now - mStart;
	std::chrono::milliseconds dif = std::chrono::duration_cast<std::chrono::milliseconds>(difference);

	return dif.count();
	
}
