#include "stdafx.h"
#include "TimerManager.h"


std::map<std::string, Timer> TimerManager::mTimerMap;

TimerManager::TimerManager()
{
}


TimerManager::~TimerManager()
{
}

void TimerManager::StartTimer(std::string timerName)
{
	if (TimerManager::mTimerMap.find(timerName) == TimerManager::mTimerMap.end())
	{
		TimerManager::mTimerMap[timerName].Start();
	}
}

void TimerManager::StopTimer(std::string timerName)
{
	if (TimerManager::mTimerMap.find(timerName) != TimerManager::mTimerMap.end())
		TimerManager::mTimerMap.erase(timerName);
}

long long TimerManager::ReadTimer(std::string timerName)
{
	return TimerManager::mTimerMap[timerName].Lap();
}
