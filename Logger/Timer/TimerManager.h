#pragma once
#include<string>
#include<map>
#include"Timer.h"
class TimerManager
{
public:
	TimerManager();
	~TimerManager();

	static void StartTimer(std::string);
	static void StopTimer(std::string);
	static long long ReadTimer(std::string);


private:
	static std::map<std::string, Timer> mTimerMap;

};

