#pragma once
#include<chrono>
class Timer
{
	std::chrono::time_point<std::chrono::system_clock> mStart;
public:
	Timer();
	~Timer();

	void Start();
	long long Lap();
};

